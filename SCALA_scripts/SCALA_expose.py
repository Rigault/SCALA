#!/usr/bin/python

import sys

import numpy       as N
import SCALA_tools as St
import SNIFS_Exposure_tools as SEt

def Exposure_run(Wavelengths_array,Expo_time_array,
                 snifs_mode,savefile,
                 use_clap=[True,True],verbose=True):
    """
    """
    
    if verbose:
        print "".center(50,'*')
        print Wavelengths_array,Expo_time_array
        print "".center(50,'*')

    if type(Wavelengths_array) !=float and   type(Wavelengths_array)!= int:
        if len(Wavelengths_array) != len(Expo_time_array) or len(Wavelengths_array)==0:
            error_message = "ERROR SCRIPT INPUT [SCALA_test] -- opts.test_wavelengthes and opts.test_exposure_times must have the same size and by greater than 0"
            if erbose:
                print error_message
            if snifs_mode:
                print "Status 1"
                sys.exit(1)
            else:
                raise ValueError(error_message)
    else:
        Wavelengths_array = N.asarray([Wavelengths_array],dtype='float')
        Expo_time_array   = N.asarray([Expo_time_array],  dtype='float')
    
    # ----------------------- #
    # -- SCALA RUN ITSELF  -- #
    # ----------------------- #
    Scala = St.SCALA(Wavelengths_array,Expo_time_array,used_clap=[True,True],
                     verbose=verbose,snifs_mode=snifs_mode)
    
    Scala.do_exposures(savefile=savefile)
    Scala.Disconnect_claps()
    print "Status 0"
    sys.exit(0)


    
    
    
    
##############################################################
# ---------------------------------------------------------- #
# ----------- MAIN SCALA's SCRIPT      --------------------- #
# ---------------------------------------------------------- #
##############################################################

if __name__ == '__main__':

    import optparse
    parser = optparse.OptionParser()
    parser.add_option("-w","--wavelengths", 
                      help="Give a wavelengths",
                      default=None)
    
    parser.add_option("-e","--exposure_times", 
                      help="Give a wavel",
                      default=None)
    
    parser.add_option("-s","--savefile", 
                      help="Name of the fits file where this SCALA's run will be registered. If None, SCALA_YY_DDD_HH_MM.fits ",
                      default='SAVE_FILE_default_SCALA')

    parser.add_option("--stepRun", type="float",
                      help="Set the amplitude [in Angstrom] of the step between two SNIFS run [%default]",
                      default=150.)


    parser.add_option("-V","--Verbose",action="store_true", 
                      help="If you want all the additional prints",
                      default=False)
    parser.add_option("--snifs",action="store_true",
                      help="This option if there for print for SNIFS scripts",
                      default=False)
    

    opts,args = parser.parse_args()

    if opts.snifs:
        opts.verbose=False
        
    # ---------------------- #
    # -- OUTPUTS          -- #
    # ---------------------- #
    if opts.savefile is None:
        loc        = time.localtime()
        savefile   = "SCALA_test_%s_%d_%d_%d.fits"%(str(loc.tm_year)[-2:], loc.tm_yday,loc.tm_hour, loc.tm_min)
    else:
        savefile = opts.savefile
    
    # ---------------------- #
    # -- OUTPUTS          -- #
    # ---------------------- #  
    Wavelengths_array  = SEt.Read_wavelength_input(opts.wavelengths,
                                                   verbose=opts.Verbose,
                                                   ExtraRunStep=opts.stepRun)
    
    if Wavelengths_array is None:
        # --- No need for new wavelength -- #
        #    SCALA's jobs is done           #
        print "Status 10"
        sys.exit(0)

    
    Expo_time_array    = SEt.Read_exposure_input(opts.exposure_times,wavelength_array=Wavelengths_array,
                                                   verbose=opts.Verbose)
    
    # -- Ok do exposures -- #
    Exposure_run(Wavelengths_array,Expo_time_array,
                 opts.snifs,opts.savefile,
                 use_clap=[True,True],verbose=opts.Verbose)
