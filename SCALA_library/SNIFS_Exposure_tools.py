#!/usr/bin/python

import numpy       as N
import IO_SCALA    as IOs

#################################
# -- BASICS INPUT           --- #
#################################
IntraRun_Incremantation = 500. # Next wavelength step inside the same run (i.e., before SNIFS' readout)
ExtraRun_Incremantation = 150.  # Next wavelength step for the next run (i.e., after SNIFS' readout)
# -- SNIFS B channel -- #
B_channel_first_lbda    = 3200.
B_channel_last_lbda     = 5200.
# -- SNIFS R channel -- #
R_channel_first_lbda    = 5000.
R_channel_last_lbda     = 10000.

#################################
# -- ARRAY GENERATIONS      --- #
#################################
# ------------------- #
# -- Wavelength    -- #
# ------------------- #
def get_ExtraRunStep(ExtraRunStep_In=None):
    """
    """
    if ExtraRunStep_In is None:
        return ExtraRun_Incremantation
    return ExtraRunStep_In
        
def Read_wavelength_input(wavelength_input,verbose=True,ExtraRunStep=None):
    """
    This functions understand predefined value like B_1, B_2....
    """
    ExtraRunStep = get_ExtraRunStep(ExtraRunStep)
    if ',' in wavelength_input:
        if verbose:
            print "INFO [SCALA_expose.Read_wavelength_input] you give manual Wavelengths (',')"
        try:
            wavelength_array = N.asarray(wavelength_input.split(','),dtype="float")
        except:
            raise ValueError("The Manual Wavelength input is unparsable in float. It shoud be lbda1,lbda2,..,lbdaN [in Angstrom]")
        return wavelength_array

    if ':' in wavelength_input:
        if verbose:
            print "INFO [SCALA_expose.Read_wavelength_input] you give range Wavelengths ('Min:Max')"
        try:
            wavelength_array = N.asarray(wavelength_input.split(':'),dtype="float")
        except:
            raise ValueError("The Manual Wavelength input is unparsable in float. It shoud be lbda_min:lbda_max [in Angstrom]")
        if len(wavelength_array)!=2:
            raise ValueError("The Manual Wavelength input is unparsable MUST BE 2 WAVELENGTHS: lbda_min:lbda_max [in Angstrom]")

        minW,maxW = N.min(wavelength_array),N.max(wavelength_array)
        wavelength_array = N.linspace(minW,maxW,(maxW-minW)/ExtraRunStep+1)
        
        return wavelength_array

    # --------------------------- #
    # - Updated parse version   - #
    # - Allow any kind of b/r*ith  #
    # ith don't have limited size #
    # --------------------------- #
    
    if wavelength_input[0].lower() == 'b' or wavelength_input[0].lower() =='r':
        channel,ith = parse_SNIFS_BR_input(wavelength_input)
        return Get_ith_array(channel,ith,ExtraRunStep)

        # -- 1 Wavelength -- #
    if type(wavelength_input) == str:
        return N.float(wavelength_input)
    
    print "wavelength_input",wavelength_input,len(wavelength_input)
    raise ValueError("Sorry I do not understand the given wavelength input")




def Get_ith_array(channel,ith_incrementation,ExtraRunStep_):
    """
    """
    if ith_incrementation*ExtraRunStep_ >= IntraRun_Incremantation:
        # -- This means that the calibration of this wavelength has already been made -- #
        return None
    if channel.lower() == 'b':
        first_lbda = B_channel_first_lbda
        NumPoints = N.ceil((B_channel_last_lbda-B_channel_first_lbda)/IntraRun_Incremantation)
        
    else:
        first_lbda = R_channel_first_lbda
        NumPoints = N.ceil((R_channel_last_lbda-R_channel_first_lbda)/IntraRun_Incremantation)
    
    return (N.arange(NumPoints))*IntraRun_Incremantation + first_lbda +(ith_incrementation*ExtraRunStep_)

    
def parse_SNIFS_BR_input(BRinput):
    """
    """
    channel,info_ith = BRinput[0],BRinput[1:]
    for i in range(len(info_ith)):
        try:
            ith = int(info_ith[-(i+1):])
        except:
            break
        
    return channel.lower(),ith

# ------------------- #
# -- Exposure time -- #
# ------------------- #

def Read_exposure_input(exposure_input,wavelength_array=None,verbose=True):
    """
    """
    # ------------------------------- #
    # -- Pre defined Exposure times - #
    # ------------------------------- #
    if exposure_input is None:
        if wavelength_array is None:
            raise ValueError("If exposure_input is None, you need to give a wavelength_array so I can measure the exposure time")
        return get_wavelength_exposure_time(wavelength_array)

     # ------------------------------- #
    # --  Constant  exposure times -- #
    # ------------------------------- #
    if type(exposure_input) == float or type(exposure_input) == int or type(exposure_input) == str or len(exposure_input) == 1:
        if type(exposure_input) == str:
            exposure_input = N.float(exposure_input)
        elif type(exposure_input) == float or type(exposure_input) == int:
            exposure_input = float(exposure_input)
        else:
            exposure_input = exposure_input[0]
            
        if verbose:
            print "INFO [SCALA_expose.Read_exposure_input] You requested *one constant* exposure time"
        if wavelength_array is None:
            if verbose:
                print "INFO [SCALA_expose.Read_exposure_input] You requested *one constant* exposure time but no wavelength array, so a float is returned"
            return exposure_input
        if type(wavelength_array) == float:
            return N.float(exposure_input)
        
        return N.ones(len(wavelength_array)) * N.float(exposure_input)
    
    # ------------------------------- #
    # -- Force input exposure times - #
    # ------------------------------- #
    if ',' in exposure_input:
        if verbose:
            print "INFO [SCALA_expose.Read_exposure_input] you give manual Exposure time (',')"
        try:
            exposure_array = N.asarray(exposure_input.split(','),dtype="float")
        except:
            raise ValueError("The Manual exposure time input is unparsable in float. It shoud be expo1,expo2,..,expoN [in seconde]")
        return exposure_array
    
   

    
def get_wavelength_exposure_time(wavelengths):
    """
    """
    fileExpo = N.asarray( [l.split() for l in open(IOs.INPUT_data+"exposure_time.txt").read().splitlines() if l[0]!="#"],dtype='float').T
    if type(wavelengths) == float:
        i = N.argmin(N.abs(fileExpo[0]*10-wavelengths))
        return fileExpo[1][i]
    # -- its an array then -- #
    return N.asarray( [ fileExpo[1][N.argmin(N.abs(fileExpo[0]*10-lbda))] for lbda in wavelengths])
