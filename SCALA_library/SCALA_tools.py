#!/usr/bin/python

import pyfits as F

import Monochromator_tools as Mt
import Clap_tools          as Cl
import IO_SCALA            as IOs
from General_tools import timeout

import time
import sys
import numpy as N
import warnings
warnings.filterwarnings('ignore')


"""
VERSION: 18th Octobre

What does SCALA needs to run an experiment.


SCALA:
------
   - lamda         
   - exposure time 
   
   -> MonoChrometor
   -> CLAP (number of claps)
   -> LAMP

   => Spectra (+ header)
   
Monochrometors:
   - Wavelength -- Only VARIABLE
   - Shutter (open / close)
   - Filter  (2 filters, 3 cases (case without filter))
   - Grating

CLAP:
   - Line_periods -- ONLY VARIABLE (=exposure time in an integer number of line_frequency)
   - Frequency
   - Channels (mainly gain)
   - line_frequency (number fixed 50Hz in Fr 60Hz in US)

IO:
    - Mirror_lamp (Xe or Halo)
    - On/Off (Mono/lamps/Clap)

"""


class SCALA():
    """
    """
    def __init__(self,wavelength_array,exptime_array,
                 used_clap=[True,True],safemode=False,
                 verbose=True,snifs_mode=False):
        """
        """
        self.wavelength_array = wavelength_array
        self.exptime_array    = exptime_array
        self.used_clap        = used_clap
        self.safemode         = safemode
        self.verbose          = verbose
        self.snifs_mode       = snifs_mode
        if self.snifs_mode:
            self.verbose = False
            
        #self._test_inputs_()
        # -------------------------- #
        # -- Load the instruments -- #
        # -------------------------- #
        self._setup_scala_()
        
        #else:
        #    try:
        #        self._setup_scala_()
        #    except:
        #        if self.verbose:
        #            print "SCALA failed to init. Let's try again"
        #        try:
        #            self._setup_scala_()
        #        except:
        #            raise ValueError("SCALA loading Failed")
    
        self._check_up_()
        if self.snifs_mode:
            print "SCALA loaded"
            sys.stdout.flush()
            

    # ------------------------ #
    # -  Internal tools      - #
    # ------------------------ #
    ################
    #  SETUPS      #
    ################
    #@timeout(IOs.SCALA_max_loading_time)
    def _setup_scala_(self):
        """
        """
        self._setup_MonoChromator_()
        self._setup_Claps_()
        self._setup_IO_()
        self.Key_is_scala_ready = False
        
    def _setup_MonoChromator_(self,lbda=None):
        """
        This Will define "self.Mono"
        """
        self.Mono = Mt.MonoChromator(lbda,verbose=self.verbose)

    def _setup_Claps_(self,line_periods=None,**kwargs):
        """
        This Will define "self.Clap0" and "self.Clap1"
        --> see used_clap to know which is/are used
        
        **kwargs = {frequency=250,Line_frequency=None,
                    channels="b0",auto_connexion=True,
                    verbose=True}
                    
        --> see Clap_tools.CLAP.__init__()
        
        """
        # Default Line_periods = Line_frequency, i.e. 1 second #
        if line_periods is None:
            line_periods = IOs.Line_frequency

        # --- CLAP 0 --- #
        if self.used_clap[0]:
            self.Clap0 = Cl.CLAP(0,line_periods,
                                 verbose=self.verbose,
                                 **kwargs)
        else:
            self.Clap0 = None
            
        # --- CLAP # --- #            
        if self.used_clap[1]:
            self.Clap1 = Cl.CLAP(1,line_periods,
                                 verbose=self.verbose,
                                 **kwargs)
        else:
            self.Clap1 = None

    def _setup_IO_(self):
        """
        """
        self.IO = IOs.IO_scala(verbose=self.verbose)
        
    ##############################
    #  SCALA CHECK UPS           #
    ##############################
    def _check_up_(self):
        """
        """
        if self.safemode is False:
            if self.verbose:
                print "No Check up"
                sys.stdout.flush()
                
            self.Key_is_scala_ready = True
            return
        
        if self.verbose:
            print "*** SCALA._check_up_: WARNING THE IO TEST IS NOT COMPLET YET ***"
        
        if self.IO.Is_MonoChrometor_on is False:
            if self.snifs_mode:
                sys.exit(IOs.Error_status_Mono)
            else:
                print "SCALA._check_up_(): The MonoChromator is off"
                self.Key_is_scala_ready = False
                return
            
        if self.IO.Is_XeLamp_on is False:
            if self.snifs_mode:
                sys.exit(IOs.Error_status_Xe)
            else:
                print "SCALA._check_up_(): The Xenon Lamp is off"
                self.Key_is_scala_ready = False
                return
        # ------------------------ #
        # MonoChromator          - #
        # ------------------------ #
        if self.Mono.Is_Ready() is False:
            if self.Mono.Is_Ready() is False:
                if self.snifs_mode:
                    print "Status %d"%IOs.Error_status_Mono
                    sys.exit(IOs.Error_status_Mono)
                else:
                    print "SCALA._check_up_(): The MonoChromator is not ready"
                    self.Key_is_scala_ready = False
                    return

        # ------------------------ #
        # CLAP                   - #
        # ------------------------ #
        if self.verbose:
            print "*** SCALA._check_up_: WARNING: No Test for Clap ***"
        if self.Clap0 is None and self.Clap1 is None:
            print "SCALA._check_up_(): No Clap avialable, both are None"
            self.Key_is_scala_ready = False
            return
        
        self.Key_is_scala_ready = True
        
    ##############################
    #  Internal CLAP functions   #
    ##############################
    def _Set_claps_observation_time_(self,new_observation_time_in_sec):
        """
        """
        new_line_periods = new_observation_time_in_sec*IOs.Line_frequency
        
        if self.Clap0 is not None:
            self.Clap0.change_observation_time( int(new_line_periods))
            self.Effective_clap_exposure_time = self.Clap0.exposure_time_in_second
        if self.Clap1 is not None:
            self.Clap1.change_observation_time( int(new_line_periods))
            self.Effective_clap_exposure_time = self.Clap1.exposure_time_in_second
            
    def _Prepare_claps_(self):
        """
        """
        if self.Clap0 is None and self.Clap1 is None:
            print "SCALA._Prepare_claps_(): No Clap avialable, both are None"
            self.Key_is_scala_ready = False
            
        if self.Clap0 is not None:
            self.Clap0.prepare_the_ADC()
            
        if self.Clap1 is not None:
            self.Clap1.prepare_the_ADC()

    def _read_out_claps_ADC_(self,wait_unit_the_end=True):
        """
        """
        if self.Clap0 is not None:
            self.Clap0.read_out_ADC()
            
        if self.Clap1 is not None:
            self.Clap1.read_out_ADC()
            
        if wait_unit_the_end:
            # -- Do not do anything until the clap did the exposure -- #
            time.sleep(self.Effective_clap_exposure_time)
        
    def _get_data_from_claps_ADC_(self):
        """
        """
        self.Claps_data    = []
        self.Claps_timings = []
        
        if self.Clap0 is not None:
            self.Clap0.get_data_from_ADC()
            #data_C0   = self.Clap0.get_mean_data()
            data_C0   = [self.Clap0.mean_data,  self.Clap0.std_data,
                         self.Clap0.median_data,self.Clap0.mean_clipped,
                         self.Clap0.nMAD_data,
                         self.Clap0.npts_data,  self.Clap0.dmean_data]
                
            Timing_C0 = [self.Clap0.time_readout,self.Clap0.time_ADC_to_FIFO_starts]
        else:
            data_C0   = [N.NaN]*7
            Timing_C0 = [N.NaN,N.NaN]
            
            
        if self.Clap1 is not None:
            
            self.Clap1.get_data_from_ADC()
            #data_C1   = self.Clap1.get_mean_data()
            data_C1   = [self.Clap1.mean_data,  self.Clap1.std_data,
                         self.Clap1.median_data,self.Clap1.mean_clipped,
                         self.Clap1.nMAD_data,
                         self.Clap1.npts_data,  self.Clap1.dmean_data]
            
            Timing_C1 = [self.Clap1.time_readout,self.Clap1.time_ADC_to_FIFO_starts]
        else:
            data_C1   = [N.NaN]*7
            Timing_C1 = [N.NaN,N.NaN]

        self.Claps_data    = [data_C0,    data_C1]
        self.Claps_timings = [Timing_C0,Timing_C1]


    def _record_clap_data_(self):
        """
        """
        if ("Timing_observation" in dir(self)) is False:
            self.Timing_observation = []
        if ("Clap_data1" in dir(self)) is False:
            self.Clap_data1 = []
        if ("Clap_data0" in dir(self)) is False:
            self.Clap_data0 = []

        if len(self.Timing_observation_tmp) != 3:
            print "Timing_observation_tmp"
        if len(self.Clap_data0_tmp) != 3:
            print "Clap_data0"
            print len(self.Clap_data0_tmp)
        if len(self.Clap_data1_tmp) != 3:
            print "Clap_data1"
            
        # --------------------- #
        # - From tmp to saved - #
        # --------------------- #
        # -- Timings -- #
        for tmp_timing in self.Timing_observation_tmp:
            self.Timing_observation.append(tmp_timing)
        # -- Clap0 -- #            
        for tmp_clap0 in self.Clap_data0_tmp:
            self.Clap_data0.append(tmp_clap0)
        # -- Clap1 -- #
        for tmp_clap1 in self.Clap_data1_tmp:
            self.Clap_data1.append(tmp_clap1)

            
        # ----------------- #
        # - Re init the tmp #
        self.Timing_observation_tmp = []
        self.Clap_data0_tmp         = []
        self.Clap_data1_tmp         = []
        
    def _tmp_record_clap_data_(self,used_wavelength,used_expotime):
        """
        This TMP recording enable to break the exposure if the loop failed
        but still the previous good data will be saved.
        This way only 3 component at the time are saved.

        -> See _record_clap_data_
        """
        if ("Timing_observation_tmp" in dir(self)) is False:
            self.Timing_observation_tmp = []
        
        self.Timing_observation_tmp.append([self.TIME_Star_Exposure,
                                        self.TIME_Ends_Exposure])
        
        if ("Clap_data1_tmp" in dir(self)) is False:
            self.Clap_data1_tmp = []
        if ("Clap_data0_tmp" in dir(self)) is False:
            self.Clap_data0_tmp = []

        self.Clap_data0_tmp.append(N.concatenate([[used_wavelength,used_expotime],
                                # -- This is the First Clap -- #
                                self.Claps_data[0]]))
        
        self.Clap_data1_tmp.append(N.concatenate([[used_wavelength,used_expotime],
                               # -- This is the Second Clap -- #
                                self.Claps_data[1]]))
        
        
        
    ############################
    # -  SCALA METHODS       - #
    ############################
    def Disconnect_claps(self):
        """
        """
        if self.Clap0 is not None:
            self.Clap0.disconnect_me()
            self.Clap0 = None
            
        if self.Clap1 is not None:
            self.Clap1.disconnect_me()
            self.Clap1 = None
        
        if self.Clap0 is None and self.Clap1 is None:
            if self.verbose:
                print "WARNING --  No Clap avialable, both are None"
                
            self.Key_is_scala_ready = False
            
    def Connect_claps(self,used_clap):
        """
        used_clap must be [bool,bool]
        """
        self.Disconnect_claps()
        self.used_clap=used_clap
        self._setup_Claps_()

    def writeto(self,savefile,clobber=True):
        """
        savefile add the ".fits"
        """
        import pyfits as F
        
        if ("Clap_data0" not in dir(self)):
            if self.snifs_mode:
                sys.exit(IOs.Error_status_Scala)
            else:
                print "ERROR [SCALA.writeto] you never run 'do_exposures', NO DATA TO SAVE"
                return
        
        for i,clap_data in enumerate([self.Clap_data0.T,self.Clap_data1.T]):

            if i == 0:
                clap = self.Clap0
            elif i ==1:
                clap = self.Clap1
            else:
                raise ValueError("ONLY 2 CLAP, WHY AM I HERE ???")
            
            # ----------- #
            # remember, clap_data = [lbda,expoTime,Clap signal (ADU), error on Clap signal ( ADU)
            #self.Clap1.mean_data,  self.Clap1.std_data,
            #             self.Clap1.median_data,self.Clap1.nMAD_data,
            #             self.Clap1.npts_data,  self.Clap1.dmean_data
            lbda_,expo_, mean_data_,std_data_,median_data_,mean_clip_data_,nMAD_data_,npts_data_,dmean_data_ = clap_data
            hdu = F.PrimaryHDU(mean_data_)
            # -- Note format E = float, D = double, L = bool, I = interger, A = Charactere -- #
            col_lbda   = F.Column(name='WAVELENGTH', format='E', array=lbda_,unit="Angstrom")
            col_expo   = F.Column(name='EXPTIME',    format='E', array=expo_,unit="second")

            col_std    = F.Column(name='STD',        format='D', array=std_data_,unit="ADU")
            col_median = F.Column(name='MEDIAN',     format='D', array=median_data_,unit="ADU")
            col_mean_C = F.Column(name='MEAN_CLIP',  format='D', array=mean_clip_data_,unit="ADU")
            col_nMAD   = F.Column(name='nMAD',       format='D', array=nMAD_data_,unit="ADU")
            col_error  = F.Column(name='ERROR',      format='D', array=dmean_data_,unit="ADU")
            col_npts   = F.Column(name='N_POINTS',   format='D', array=npts_data_,unit="ADU")
            
            col_start  = F.Column(name='TIME_START', format='D', array=self.Timing_observation.T[0])
            col_stop   = F.Column(name='TIME_STOP',  format='D', array=self.Timing_observation.T[1])
            tbhdu      = F.new_table([col_lbda, col_expo,col_std,col_median,col_mean_C,
                                      col_nMAD, col_error,col_npts,col_start,col_stop])
            # ------------------- #
            # -- Primery HDU   -- #
            # ------------------- #
            hdu.header.update("NAXIS",1,"Number of dimension per HDU")
            hdu.header.update('NAXIS1',len(lbda_),"Number of Wavelength bins, see 'WAVELENGTH' Table")
            hdu.header.update("CDELT1",lbda_[1]-lbda_[0],"CAUTION COULD BE VARIABLE, see 'WAVELENGTH' Table")
            hdu.header.update("CRVAL1",lbda_.min(),"Smaller wavelength, see 'WAVELENGTH' Table")
            hdu.header.update("UNITS","ADU","Analog to Digital Unit (integers)")
            # -- Tables information -- #
            hdu.header.update("TABLES",5,"Number of tables associated to the PrimaryHDU")
            hdu.header.update("TABLES1","WAVELENGTH","Proper Wavelength array used")
            hdu.header.update("TABLES2","EXPTIME","Proper Exposure time array used (in second)")
            hdu.header.update("TABLES3","STD","Standard deviation for Primary HDU Data")
            hdu.header.update("TABLES4","MEDIAN","Median instead of the mean in Primary HDU Data")
            hdu.header.update("TABLES5","MEAN_CLIP","Mean clipped (%d sigma) instead of the mean in Primary HDU Data"%clap.clipping_sigma)
            hdu.header.update("TABLES6","nMAD","nMAD for Primary HDU Data")
            hdu.header.update("TABLES7","ERROR","Error associated to the Primary HDU")
            hdu.header.update("TABLES8","N_POINTS","Number of point used to define the Primary HDU/nMAD..")
            
            hdu.header.update("TABLES9","TIME_START","time.time() just *before* the ADC read_out")
            hdu.header.update("TABLES10","TIME_STOP","time.time() just *after* the ADC read_out")

            # -- Observation parameters -- #
            # --------------- #
            # -- Clap info -- #
            if self.Clap0 is None:
                hdu.header.update("CLAP0",0,"1 if used, 0 if not")
            else:
                hdu.header.update("CLAP0",1,"1 if used, 0 if not")
            if self.Clap1 is None:
                hdu.header.update("CLAP1",0,"1 if used, 0 if not")
            else:
                hdu.header.update("CLAP1",1,"1 if used, 0 if not")
            
           
            hdu.header.update("CLAP_ID",clap.device_number,"Clap device 0 or 1")
            hdu.header.update("CLAPFREQ",clap.frequency,"Number of points per second taken by the CLAP (in kHz)")
            hdu.header.update("LINEFREQ",clap.Line_frequency,"Frequency of the electric Lines (50Hz=EU / 60Hz=US)")
            hdu.header.update("CLAPCHAN",clap.channels,"b0 = gain of 1;b0 = gain of 32, ... see Clap_tools ")
            # ------------------- #
            # -- DataFormat    -- #
            # ------------------- #
            hdu.header.update("DATAFMT","bkg_DATA_bkg","You record 1 background point after and before the exposure")
            # ------------------- #
            # -- MonoChromator -- #
            # ------------------- #
            hdu.header.update("MONOGRAT",None,"Variable, See 'WAVELENGTH' table and MonoChromator_tools.py")
            hdu.header.update("MONOFILT",None,"Variable, See 'WAVELENGTH' table and MonoChromator_tools.py")
            hdu.header.update("LAMP",None,"Variable, Xenon Lamp if lbda smaller than 7000 / Halo otherwise")
            # ------------------- #
            # -- General_Run   -- #
            # ------------------- #
            hdu.header.update("FAILURE",self._Failure_flag_,"None = All went Fine, Wavelengths show which failed, -1 Means Epic Failure")
            ###############
            #  END        #
            ###############
            hdulist = F.HDUList([hdu, tbhdu])
            if len(savefile.split(".")) >1 and savefile.split(".")[-1]=="fits":
                savefile_name = "scala_clap%d_%s"%(clap.device_number,savefile)
            else:
                savefile_name = "SC%d_%s.fits"%(clap.device_number,savefile)
            
            hdulist.writeto(savefile_name, clobber=clobber)
            

        
            
    ############################
    # -  SCALA MAIN TOOLS    - #
    ############################
    def Take_background(self,exposure_time,try_again_if_fails=True):
        """
        """
        if self.Key_is_scala_ready is False:
            if try_again_if_fails:
                self.Take_background(exposure_time,try_again_if_fails=False)
                
            if self.snifs_mode:
                sys.exit(IOs.Error_status_Scala)
                
            if self.verbose:
                print "ERROR [SCALA.Expose]: Apparently, Scala is not ready (self.Key_is_scala_ready is False) -- Check_up Scala !"
                return
            
        # --- Update the Claps for the given exposure time --- #
        self._Set_claps_observation_time_(exposure_time)
        self._Prepare_claps_()
        # -- EXPOSURE WITHOUT OPENING THE SHUTTER
        self.TIME_Star_Exposure = time.time()
        self._read_out_claps_ADC_(wait_unit_the_end=True)
        self.TIME_Ends_Exposure = time.time()
    
    
        # -------------------------------- #
        # -- Start collecting the data  -- #
        self._get_data_from_claps_ADC_()
        # This later function Created:
        #        - Claps_data = [[mean_clap1,dmean_clap1], [mean_clap1,dmean_clap1]]
        #          not that is ClapX is None, [N.NaN,N.NaN] is set for (d)mean_clapX
        

    
    def Expose(self,wavelength,exposure_time,try_again_if_fails=True):
        """
        wavelength in Angstrom (nm*10)
        exposure_time in Second
        ------------
        CAUTION it only test if the High-level Key "self.Key_is_scala_ready" is True or not,
        It is possible --though unexpected-- that this key if True even if SCALA IS NOT READY
         -> This might notably be possible if one changes this parameters using lower level functions
             (_low_level_function_) 
        
        """
        if self.Key_is_scala_ready is False:
            if try_again_if_fails:
                self.Expose(wavelength,exposure_time,try_again_if_fails=False)
                
            if self.snifs_mode:
                sys.exit(IOs.Error_status_Scala)
                
            if self.verbose:
                print "ERROR [SCALA.Expose]: Apparently, Scala is not ready (self.Key_is_scala_ready is False) -- Check_up Scala !"
                return
        
        # --- Update the monoChromator for the given wavelength --- #
        self.Mono.change_wavelength(wavelength)
        
        # -- Mono, Are you ready ? -- 
        if self.Mono.Is_Ready() is False:
            self.Mono.CS_sleep(3)
            if self.Mono.Is_Ready() is False:
                print "".center(75,"*")
                print " SCALA [Expose]: MonoChromator is not ready. I am stoping the function ".center(75,"*")
                print "".center(75,"*")
                print "Status %d"%IOs.Error_status_Mono
                sys.exit(IOs.Error_status_Scala)
            
        # --- Update the Claps for the given exposure time --- #
        self._Set_claps_observation_time_(exposure_time)
        self._Prepare_claps_()
        
        # --------------------------- #
        # -- The exposure by itself - #
        # --------------------------- #
        
        # -- PreExposure
        self.Mono.switch_on_shutter()
        # Shutter is Open
        self.TIME_Star_Exposure = time.time()

        self._read_out_claps_ADC_(wait_unit_the_end=True)
        # Shutter is going to close
        self.Mono.switch_off_shutter()
        self.TIME_Ends_Exposure = time.time()
        # -- PreExposure
                
        # -- End of The exposure itself -- #
        # -------------------------------- #
        # -- Start collecting the data  -- #
        self._get_data_from_claps_ADC_()
        # This later function Created:
        #        - Claps_data = [[mean_clap1,dmean_clap1], [mean_clap1,dmean_clap1]]
        #          not that is ClapX is None, [N.NaN,N.NaN] is set for (d)mean_clapX
        
    def _Exposure_loop_(self,lbda,expo,background_time):
        """
        """
        # -- Is everything still Ok ? -- #
        print "   (I am changing the wavelength)"
        self.Mono.change_wavelength(lbda)
        # ---------------------------- #
        # --    PRE-BACKGROUND      -- #
        self.Take_background(background_time)
        self._tmp_record_clap_data_(lbda,expo)
        print "   (Background taken - Exposure ongoing)"
        sys.stdout.flush()
        # ------------------------------ #
        # --  OBSERVATION BY ITSELF   -- #
        self.Expose(lbda,expo)
        self._tmp_record_clap_data_(lbda,expo)
        print "   (Exposure taken - 2nd Background ongoing)"
        sys.stdout.flush()
        # ------------------------------ #
        # --    AFTER-BACKGROUND      -- #
        self.Take_background(background_time)
        self._tmp_record_clap_data_(lbda,expo)
        print "   (2nd Background taken -> Data saving)"
        sys.stdout.flush()
            
        
    def do_exposures(self,wavelength_array=None,exptime_array=None,
                     background_time=1,
                     savefile=None):
        """
        This function scans the wavelength_array and exptime_array and "Expose" if time.
        if savefile is not None, This calls self.writeto(savefile+".fits") -> This save the (mean)clap data
        
        --------
        Clap data are registered in 
        """
        # --------- INPUTS --------- #
        if wavelength_array is not None:
            self.wavelength_array = wavelength_array
            
        if exptime_array is not None:
            self.exptime_array = exptime_array

        # -- Does it make sense ? -- #
        if N.shape(self.wavelength_array) != N.shape(self.exptime_array) or N.shape(self.exptime_array) ==():
            if self.snifs_mode:
                print "Wavelength--Exposure time issue"
                print "Status %d"%IOs.Error_status_Scala
                sys.exit(IOs.Error_status_Scala)
            else:
                raise ValueError("Either wavelength_array do not have the same shape as exptime_array, or both are None...")
        
        # --     So let's go      -- #
        self._Failure_flag_ = None
        for i,lbda in enumerate(self.wavelength_array):
            expo = self.exptime_array[i]
            print "%d/%d - %.1f A - %d second expo"%(i+1,len(self.wavelength_array),lbda,expo)
            sys.stdout.flush()
            # --------------------- #
            # -- Check if Mono Ready - #
            # --------------------- #
            if self.Mono.Is_Ready() is False:
                if self.Mono.Is_Ready() is False:
                    print " ERROR -- The MonoChromator is not ready (Twice)"
                    print "Status %d"%IOs.Error_status_Mono
                    print "  I stop the function. Data will be saved."
                    break
            # --------------------- #
            # -- Loop with saving - #
            # --------------------- #
            try:
                self._Exposure_loop_(lbda,expo,background_time)
                
            except:
                print "  This exposure loop %d/%d FAILED. Data of this loop not recorded."%(i+1,len(self.wavelength_array))
                if hasattr(self._Failure_flag_, '__iter__'):
                    self._Failure_flag_.append(lbda)
                else:
                    self._Failure_flag_ = [lbda]
                # -- Better to restore the MonoChromator -- #
                print "   I am running the recovery of the monochromator"
                sys.stdout.flush()
                try:
                    self.Mono._restore_MonoChromator_(self.wavelength_array[i])
                except:
                    print "MAJOR FAILURE... I Quite the Exposure loop and register what we already have !"
                    self._Failure_flag_ = -1
                    break
                
                # -- Clear the tmp data  -- 1
                self.Timing_observation_tmp = []
                self.Clap_data0_tmp         = []
                self.Clap_data1_tmp         = []
                
                continue
            
            # -- If everything went well, record the data -- #
            self._record_clap_data_()
            print "   (Data saved)"
            sys.stdout.flush()
        
        self.Clap_data0 = N.asarray(self.Clap_data0)
        self.Clap_data1 = N.asarray(self.Clap_data1)
        self.Timing_observation = N.asarray(self.Timing_observation)
        # ----------------- #
        # -- SAVING DATA -- #
        # ----------------- #
        if savefile is False:
            print "WARNING [SCALA.do_exposures] -- No Fits file has been registered, savefile is False"
            return
        
        if savefile is None:
            if self.verbose:
                print "INFORMATION [SCALA.do_exposures] -- Default savefile value assigned to the fits file"
            savefile= IOs.assign_prod_name()
            print "       --> %s"%savefile
            
        self.writeto(savefile)
